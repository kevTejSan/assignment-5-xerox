/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.xerox;

import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import com.assignment5.xerox.data.DataStore;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import com.assignment5.entities.Customer;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import com.assignment5.entities.SalesPerson;
import com.assignment5.xerox.data.DataStore;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * all methods for performing analysis as per the problem statements will go here
 * @author tejas
 */
public class Analytics {

    private static Map<Integer,Double> errorMap;
     private static Map <Integer, Double> avgSalePriceMap;
    public static void bestNegotiatedProd(){
        System.out.println("1)");
        int quantityCount=0;
        int prodId=0;
        int preValue=0;
        int newValue=0;
        int pid=0;
        Map<Integer,Integer> outputMap = new HashMap<>();
        Map<Integer,Integer> orederPro = new HashMap<>();
        Map<Integer,Order> orders = DataStore.getInstance().getOrders();
        Map<Integer,Product> products = DataStore.getInstance().getProducts();
        Map<Integer,Product> prodMap = DataStore.getInstance().getProducts();  
        for(int productID : prodMap.keySet()){
               outputMap.put(productID, 0);
        }  
        for (int orderId : orders.keySet()) {
               Order order = orders.get(orderId);
               prodId=order.getItem().getProductId();
             
               if(order.getItem().getSalesPrice() > products.get(prodId).getTarget() ){
                   
                   quantityCount=order.getItem().getQuantity();
                   orederPro.put(orderId,quantityCount ); 
               }                                 
        }       
        for(int ordId : orederPro.keySet()){
            Order order = orders.get(ordId);
            int productId = order.getItem().getProductId();
            
            preValue= outputMap.get(productId);
            newValue=preValue+orederPro.get(ordId);
            outputMap.put(productId, newValue);
            
        }  
        //System.out.println("All products with sales price higher than target price:");
        List<Map.Entry<Integer,Integer>> sortOutputList = new LinkedList<>(outputMap.entrySet());
        Collections.sort(sortOutputList, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> t, Map.Entry<Integer, Integer> t1) {
                return t1.getValue().compareTo(t.getValue());
            }
        });   
        
        ArrayList<Integer> arrayList = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry : sortOutputList) {
            //System.out.println(prodMap.get(entry.getKey()) + " with measuring value: "+entry.getValue());
            arrayList.add(entry.getValue());
        }       
       
       Map<Integer, Integer> hm = new HashMap<Integer, Integer>(); 
          for(int i : arrayList){
              Integer j = hm.get(i); 
            hm.put(i, (j == null) ? 1 : j + 1);      
          }   
         List<Map.Entry<Integer,Integer>> last = new LinkedList<>(hm.entrySet());
        Collections.sort(last, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> t, Map.Entry<Integer, Integer> t1) {
                return t1.getKey().compareTo(t.getKey());
            }
        });   
        System.out.println("Our Top 3 Best Negotiated Products:");
            int key=0;
            int value=0;
            int z=0;    
         for (Map.Entry<Integer, Integer> entry : sortOutputList) {
             
             if(!(entry.getValue()==value))
             {
                 z++;
                 System.out.println();
             }
             int v = entry.getValue();
             //int c = hm.get(v);
                if(z<4){
                    for(int l : arrayList ){  
                    if(v==l & !(key==entry.getKey())){       
                    System.out.println(prodMap.get(entry.getKey()) + " With Quantity: "+entry.getValue());
                    key = entry.getKey(); 
                    value=entry.getValue();
             }                       
            }
            }  
                
                else break;
        }    
  } 

    /**
     * problem statement 2
     * Sum of absolute value of the difference between the sale price and target price of the products this customer bought.
     */
    static void threeBestCustomers() {
        System.out.println("2)");
        //hashmap with key = customer id , value = sum of abs val of (diff between target price and sale price)
        Map<Integer,Integer> outputMap = new HashMap<>();
        Map<Integer,Order> orders = DataStore.getInstance().getOrders();
        Map<Integer,Product> products = DataStore.getInstance().getProducts();
        Map<Integer,Customer> customers = DataStore.getInstance().getCustomers();
        //initialize the outpu map:
        for(int cId : customers.keySet()){
            outputMap.put(cId, 0);
        }
        for (int orderId : orders.keySet()) {
            Order order = orders.get(orderId);
            try{
            int newValue = outputMap.get(order.getCustomerId()) 
                    + Math.abs(order.getItem().getSalesPrice()
                            -products.get(order.getItem().getProductId()).getTarget()
                    );
            outputMap.put(orders.get(orderId).getCustomerId(), newValue);
        }catch(NullPointerException e){
            System.out.println("product id: "+order.getItem()
                .getProductId());
            e.printStackTrace();
        }
        }
 
        //System.out.println("All the customers:");
        List<Map.Entry<Integer,Integer>> sortOutputList = new LinkedList<>(outputMap.entrySet());
        Collections.sort(sortOutputList, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> t, Map.Entry<Integer, Integer> t1) {
                return t1.getValue().compareTo(t.getValue());
            }
        });   
        
        ArrayList<Integer> arrayList = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry : sortOutputList) {
            //System.out.println(customers.get(entry.getKey()) + " with measuring value: "+entry.getValue());
            arrayList.add(entry.getValue());
        }       
       
       Map<Integer, Integer> hm = new HashMap<Integer, Integer>(); 
          for(int i : arrayList){
              Integer j = hm.get(i); 
            hm.put(i, (j == null) ? 1 : j + 1);      
          }   
         List<Map.Entry<Integer,Integer>> last = new LinkedList<>(hm.entrySet());
        Collections.sort(last, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> t, Map.Entry<Integer, Integer> t1) {
                return t1.getKey().compareTo(t.getKey());
            }
        });   
        System.out.println("Our Top 3 Best Customer:");
            int key=0;
            int value=0;
            int z=0;    
         for (Map.Entry<Integer, Integer> entry : sortOutputList) {
             
             if(!(entry.getValue()==value))
             {
                 z++;
                 System.out.println();
             }
             int v = entry.getValue();
             //int c = hm.get(v);
                if(z<4){
                    for(int l : arrayList ){  
                    if(v==l & !(key==entry.getKey())){       
                    System.out.println(customers.get(entry.getKey()) + " With Measuring: "+entry.getValue());
                    key = entry.getKey(); 
                    value=entry.getValue();
             }                       
            }
            } 
                else break;
        }       
        
    }

    static void topThreeBestSalesPeople() {
        //problem 3
        System.out.println("3)");
        int prodProfit=0;
        int prodId=0;
        int preValue=0;
        int newValue=0;
        Map<Integer,Integer> outputMap = new HashMap<>();
        Map<Integer,Integer> orederPro = new HashMap<>();
        Map<Integer,Order> orders = DataStore.getInstance().getOrders();
        Map<Integer,Product> products = DataStore.getInstance().getProducts();
        Map<Integer,SalesPerson> salesP = DataStore.getInstance().getSalesPersons();  
        for(int sId : salesP.keySet()){
               outputMap.put(sId, 0);
        }  
        for (int orderId : orders.keySet()) {
               Order order = orders.get(orderId);
               prodId=order.getItem().getProductId();
               prodProfit= ((order.getItem().getSalesPrice()) - (products.get(prodId).getTarget())) *(order.getItem().getQuantity());
               orederPro.put(orderId, prodProfit);           
        }
         for (int orderId : orederPro.keySet() ){
            Order order = orders.get(orderId);
            int sid = order.getSupplierId(); 
            preValue= outputMap.get(sid);
            newValue= orederPro.get(orderId)+ preValue;
            outputMap.put(sid, newValue);           
        }        
        //System.out.println("All the sales people:");
        List<Map.Entry<Integer,Integer>> sortOutputList = new LinkedList<>(outputMap.entrySet());
        Collections.sort(sortOutputList, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> t, Map.Entry<Integer, Integer> t1) {
                return t1.getValue().compareTo(t.getValue());
            }
        });   
        
        ArrayList<Integer> arrayList = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry : sortOutputList) {
            //System.out.println(salesP.get(entry.getKey()) + " with measuring value: "+entry.getValue());
            arrayList.add(entry.getValue());
        }       
       
       Map<Integer, Integer> hm = new HashMap<Integer, Integer>(); 
          for(int i : arrayList){
              Integer j = hm.get(i); 
            hm.put(i, (j == null) ? 1 : j + 1);      
          }   
         List<Map.Entry<Integer,Integer>> last = new LinkedList<>(hm.entrySet());
        Collections.sort(last, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> t, Map.Entry<Integer, Integer> t1) {
                return t1.getKey().compareTo(t.getKey());
            }
        });   
        System.out.println("Our Top 3 Best Sales People:");
            int key=0;
            int value=0;
            int z=0;    
         for (Map.Entry<Integer, Integer> entry : sortOutputList) {
             
             if(!(entry.getValue()==value))
             {
                 z++;
                 System.out.println();
             }
             int v = entry.getValue();
//             int c = hm.get(v);
                if(z<4){
                    for(int l : arrayList ){  
                    if(v==l & !(key==entry.getKey())){       
                    System.out.println(salesP.get(entry.getKey()) + " With Profit: "+entry.getValue());
                    key = entry.getKey(); 
                    value=entry.getValue();
             }                       
            }
            } 
                else break;
        }    

        }

    static void totalRevenueAboveTarget() {
        //problem 4
        System.out.println("4)");
        //todo check for "above target" constraint. ask a TA if its needed
        int profitSum = 0;
        Map<Integer,Order> orders = DataStore.getInstance().getOrders();
        Map<Integer,Product> products = DataStore.getInstance().getProducts();
        for (int orderId : orders.keySet()) {
            int productId = orders.get(orderId).getItem().getProductId();
            profitSum+= (orders.get(orderId).getItem().getSalesPrice() - products.get(productId).getTarget()) * orders.get(orderId).getItem().getQuantity();
        }
        System.out.println("Our Total Revenue: "+ profitSum);
        
    }

    static void optimizeSales() {
    // problem 5
//    You need to think about the questions, including but not limited to the following questions:
//Is the target price too high or too low? 
//Is there enough gap between the target price and ceiling price?
//Is the ceiling price/targe price/floor price should be adjusted depending on your analysis?
//figure out how to use and adjust cleiling/floor price for analysis
//TODO if ceiling is not used, change the target price, so that it is within ceiling and floor and the error % is between -5% and 5%
        System.out.println("5)");
        errorMap = new HashMap<>();
        displayOriginalData("ORIGINAL");
        modifyOriginalData();
//        modifyOriginalCsv();
        displayOriginalData("MODIFIED");
    }

    private static void displayOriginalData(String dataType) {
        avgSalePriceMap = new HashMap<>(); //first sum of selling price*qty is stored, then it is used to calculate avg and the value is overwritten  
        Map<Integer,Double> avgAboveTarget = new HashMap<>();
        Map<Integer,Double> avgBelowTarget = new HashMap<>();
        Map <Integer, Integer> prodTotalQty = new HashMap<>(); //  
        Map <Integer,Product> products = DataStore.getInstance().getProducts();
        Map <Integer,Order> orders = DataStore.getInstance().getOrders();
        //initialize
        for (int prodId : products.keySet()) {
            avgSalePriceMap.put(prodId, 0d);
            prodTotalQty.put(prodId, 0);
        }
        //for every order, check in all orders and calculate average
//        for(int prodId: products.keySet()){
            for(int orderId : orders.keySet()){
                int sellingPrice = orders.get(orderId).getItem().getSalesPrice();
                int quantity = orders.get(orderId).getItem().getQuantity();
                double prevSum = avgSalePriceMap.get(orders.get(orderId).getItem().getProductId());
                double newSum = prevSum + (sellingPrice*quantity);
                int prevQty = prodTotalQty.get(orders.get(orderId).getItem().getProductId());
                int newQty = prevQty + quantity;
                prodTotalQty.put(orders.get(orderId).getItem().getProductId(), newQty);
                avgSalePriceMap.put(orders.get(orderId).getItem().getProductId(), newSum);
            }
//        }        
        //calculate the average
        for (int prodId : avgSalePriceMap.keySet()) {
            double sum = avgSalePriceMap.get(prodId);
            int qty = prodTotalQty.get(prodId);
            double avg = roundDoubleValue(sum/qty, 2);
            avgSalePriceMap.put(prodId, avg);
        }
        
        System.out.println(dataType+ " DATA:");
        System.out.println();
        System.out.println("Product Id\tAverage sale price\t\tTarget Price\t(Avg. Sales - target price)\tError%\t\t\tCeiling\t\t\tfloor");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------------------");
        
        //create two separate hashmaps for abovetarget and belowtarget        
        for (int prodId : avgSalePriceMap.keySet()) {
            if (avgSalePriceMap.get(prodId) > products.get(prodId).getTarget()) {
                //above target
                avgAboveTarget.put(prodId, avgSalePriceMap.get(prodId));
            }else if(avgSalePriceMap.get(prodId) < products.get(prodId).getTarget()){
                //below target
                avgBelowTarget.put(prodId, avgSalePriceMap.get(prodId));
            }
        }
        //create a method for sorting the hashmaps.
        List<Map.Entry<Integer,Product>> sortOutputList = new LinkedList<>(products.entrySet());
        Collections.sort(sortOutputList, new Comparator<Map.Entry<Integer, Product>>() {
            @Override
            public int compare(Map.Entry<Integer, Product> t, Map.Entry<Integer, Product> t1) {
//                return (int)Math.round((avgSalePriceMap.get(t.getKey()) - t.getValue().getTarget())
//                        -(avgSalePriceMap.get(t1.getKey()) - t1.getValue().getTarget()));
                   return ((Double)
                           (avgSalePriceMap.get(t.getKey()) - t.getValue().getTarget())
                           )
                           .compareTo(
                                   (avgSalePriceMap.get(t1.getKey()) - t1.getValue().getTarget())
                           );
            }
        });   
        
//        ArrayList<Integer> arrayList = new ArrayList<>();
//        for (Map.Entry<Integer, Integer> entry : sortOutputList) {
//            System.out.println(prodMap.get(entry.getKey()) + " with measuring value: "+entry.getValue());
//            arrayList.add(entry.getValue());
//        }       
        System.out.println();
        System.out.println("ABOVE TARGET:");
        for (Map.Entry<Integer, Product> entry : sortOutputList) {
            if(avgAboveTarget.containsKey(entry.getKey())){
                printValues(entry);
            }
        }
        System.out.println();
        System.out.println("BELOW TARGET:");
        for (Map.Entry<Integer, Product> entry : sortOutputList) {
            if(avgBelowTarget.containsKey(entry.getKey())){
             printValues(entry);
            }
        }
    }

   public static double roundDoubleValue(double value, int decimalPlaces) {
    if (decimalPlaces > 0){ 
        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(decimalPlaces, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    return value;
}

    private static void modifyOriginalData() {
        Map<Integer, Product> products = DataStore.getInstance().getProducts();
        for (int prodId : products.keySet()) {
//            int oldTarget = products.get(prodId).getTarget();
//            double error = errorMap.get(prodId);
              int newTarget = (int) Math.round(avgSalePriceMap.get(prodId)); //not for extremely big values
              products.get(prodId).setTarget(newTarget);
        }
    }

    private static void modifyOriginalCsv() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static void printValues(Map.Entry<Integer,Product> entry) {
        double error = roundDoubleValue(((entry.getValue().getTarget()-avgSalePriceMap.get(entry.getKey()))/avgSalePriceMap.get(entry.getKey()))*100,2);
        errorMap.put(entry.getKey(), error);
        System.out.println(entry.getValue().getProductId()
                +"\t\t|\t" + avgSalePriceMap.get(entry.getKey())
                +"\t\t|\t" + entry.getValue().getTarget()
                +"\t\t|\t" + roundDoubleValue(avgSalePriceMap.get(entry.getKey()) - entry.getValue().getTarget(),2)
                +"\t\t|\t" + error
                +"\t\t|\t" + entry.getValue().getMaxPrice()
                +"\t\t|\t" + entry.getValue().getMinPrice()
        );
    }
}

                