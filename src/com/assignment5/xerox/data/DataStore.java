/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.xerox.data;
import com.assignment5.entities.Customer;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import com.assignment5.entities.SalesPerson;
import java.util.HashMap;
import java.util.Map;

/**
 *singleton class, consists of:
 * hashmap orders: key = order id ,value = order object
 * hashmap products:key =product id , value = product obj
 * hashmap customer, key = cust number, val = obj
 * hashmap salesperson : hey= id, value = object
 * @author tejas
 */
public class DataStore {
    private Map<Integer,Product> products;
    private Map<Integer,Customer> customers;
    private Map<Integer,SalesPerson> salesPersons;
    private Map<Integer, Order> orders;
     
    private static DataStore dataStore;

     private DataStore(){
         orders= new HashMap();
         customers= new HashMap();
         salesPersons= new HashMap();
         products= new HashMap();
     }
     public static DataStore getInstance(){
        if(dataStore == null)
            dataStore = new DataStore();
        return dataStore;
    }
    public Map<Integer, Product> getProducts() {
        return products;
    }

    public void setProducts(Map<Integer, Product> products) {
        this.products = products;
    }

    public Map<Integer, Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(Map<Integer, Customer> customers) {
        this.customers = customers;
    }

    public Map<Integer, SalesPerson> getSalesPersons() {
        return salesPersons;
    }

    public void setSalesPersons(Map<Integer, SalesPerson> salesPersons) {
        this.salesPersons = salesPersons;
    }
    
    public Map<Integer, Order> getOrders() {
        return orders;
    }

    public void addOrder(Order orderObj) {
        orders.put(orderObj.getOrderId(), orderObj);
    }

    public void addCustomer(Customer customer) {
        customers.put(customer.getCustId(), customer);
    }

    public void addSalesPerson(SalesPerson sp) {
        salesPersons.put(sp.getSalesId(), sp);
    }
     
    public void addProducts(Product product){
        products.put(product.getProductId(),product);
    }
            
     
    
}
