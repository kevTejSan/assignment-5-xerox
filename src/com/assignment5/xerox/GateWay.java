/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.xerox;

import com.assignment5.entities.Customer;
import com.assignment5.entities.Item;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import com.assignment5.entities.SalesPerson;
import com.assignment5.xerox.data.DataReader;
import com.assignment5.xerox.data.DataGenerator;
import com.assignment5.xerox.data.DataStore;
import java.io.IOException;

/**
 *
 * @author kasai
 */
public class GateWay {
    private static String NEW_ORDER_CSV= "SalesData.csv";
    private static String NEW_PRODUCT_CSV= "ProductCatalogue.csv";
    private static String separator = ("----------------------------------------------");
    
    public static void main(String args[]) {
        initializeData();         
        //TODO call Analytics methods here
        System.out.println(separator);
        Analytics.bestNegotiatedProd();//problem 1
        System.out.println(separator);
        Analytics.threeBestCustomers();//problem 2
        System.out.println(separator);
        Analytics.topThreeBestSalesPeople();//problem 3
        System.out.println(separator);
        Analytics.totalRevenueAboveTarget();//problem 4
        System.out.println(separator);
        Analytics.optimizeSales(); //problem 5
        System.out.println(separator);
    }
    
    public static void printRow(String[] row){
        for (String row1 : row) {
            System.out.print(row1 + ", ");
        }
        System.out.println("");
    }

    private static void initializeData() {
        
        try{
        DataGenerator generator = DataGenerator.getInstance(); //for adding external pre-filled csv file, comment this line, add the path for new file in NEW_ORDER_CSV and supply NEW_ORDER_CSV to datareader
        DataReader orderReader = new DataReader(generator.getOrderFilePath());
//        DataReader orderReader = new DataReader(NEW_ORDER_CSV);
        String[] orderRow;
//        printRow(orderReader.getFileHeader());
        while((orderRow = orderReader.getNextRow()) != null){
//            printRow(orderRow);
              storeOrders(orderRow);
              storeCustomers(orderRow);
              storeSalesPersons(orderRow);
        }
//        System.out.println("_____________________________________________________________");
       // DataReader productReader = new DataReader(generator.getProductCataloguePath());  //for adding external pre-filled csv file, comment this line, add the path for new file in NEW_PRODUCT_CSV and supply NEW_PRODUCT_CSV to datareader
       DataReader productReader = new DataReader(NEW_PRODUCT_CSV); 
       String[] prodRow;
//        printRow(productReader.getFileHeader());
        while((prodRow = productReader.getNextRow()) != null){
//            printRow(prodRow);
            storeProducts(prodRow);
        }
    }catch (IOException e){
        e.printStackTrace();
    }
    }

    private static void storeOrders(String[] orderRow) {
//        Order-Id,Item-id,Product-Id,Quantity,Sales-Id,Customer-Id,Sales-Price-Per-Prod,Market-Segment

Order orderObj = new Order(Integer.parseInt(orderRow[0]), Integer.parseInt(orderRow[4]) , Integer.parseInt(orderRow[5]), new Item(Integer.parseInt(orderRow[2]),Integer.parseInt(orderRow[6]),Integer.parseInt(orderRow[3])));
        DataStore.getInstance().addOrder(orderObj);
    }

    private static void storeProducts(String[] prodRow) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
        Product productObj = new Product(Integer.parseInt(prodRow[0]), Integer.parseInt(prodRow[1]), Integer.parseInt(prodRow[2]),Integer.parseInt(prodRow[3]));
        DataStore.getInstance().addProducts(productObj);
      
    }

    private static void storeCustomers(String orderRow[]) {
        Customer customer = new Customer(Integer.parseInt(orderRow[5]));
        DataStore.getInstance().addCustomer(customer);
    }

    private static void storeSalesPersons(String orderRow[]) {
        SalesPerson sp = new SalesPerson(Integer.parseInt(orderRow[4]));
        DataStore.getInstance().addSalesPerson(sp);
    }
    
}
