/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

/**
 * add salesperson field in salesdata file
 * fields: id, arraylist of order objects
 * @author kasai
 */
public class SalesPerson {
    
    int salesId;

    public SalesPerson(int salesId) {
        this.salesId = salesId;
    }

    public int getSalesId() {
        return salesId;
    }

    public void setSalesId(int salesId) {
        this.salesId = salesId;
    }
    @Override
    public String toString() {
        return "SalesPerson{" + "SalesPersonId=" + salesId + '}';
    }
    
    
}
